# Mastodon

Please report any issues through the following link [HERE](https://gitlab.com/GitMaster210/mastodon/-/issues).

Here's my CSV file of the accounts I follow on Mastodon. I will update this file
when I add new followers, not if I remove one.
